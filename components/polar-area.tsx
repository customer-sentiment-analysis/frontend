import React from 'react';
import {
	Chart as ChartJS,
	RadialLinearScale,
	ArcElement,
	Tooltip,
	Legend,
} from 'chart.js';
import { PolarArea } from 'react-chartjs-2';

ChartJS.register(RadialLinearScale, ArcElement, Tooltip, Legend);

export const data = {
	labels: ['Non Content', 'Neutre', 'Content'],
	datasets: [
		{
			label: '# of Votes',
			data: [18, 54, 298],
			backgroundColor: ['#971B2F', '#F0B323', '#509E2F'],
			borderWidth: 1,
		},
	],
};

export function CustomerPolarArea() {
	return <PolarArea data={data} />;
}
