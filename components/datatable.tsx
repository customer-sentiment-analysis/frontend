function classNames(...classes: any) {
	return classes.filter(Boolean).join(' ');
}

export default function DataTable({ headers, body, title }: any) {
	console.log('====================================');
	console.log(title);
	console.log('====================================');
	return (
		<div className="px-4 sm:px-6 lg:px-8 pt-5">
			<div className="sm:flex sm:items-center">
				<div className="sm:flex-auto">
					<h1 className="text-xl font-semibold text-gray-900">{title}</h1>
					<p className="mt-2 text-sm text-gray-700">
						Your team is on the{' '}
						<strong className="font-semibold text-gray-900">Startup</strong>{' '}
						plan. The next payment of $80 will be due on August 4, 2022.
					</p>
				</div>
				<div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
					<button
						type="button"
						className="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
					>
						Update credit card
					</button>
				</div>
			</div>
			<div className="-mx-4 mt-10 ring-1 ring-gray-300 sm:-mx-6 md:mx-0 md:rounded py-3 font-serif">
				<table className="min-w-full divide-y divide-gray-300">
					<thead>
						<tr>
							{headers &&
								headers.map((item: any) => (
									<th
										key={item}
										scope="col"
										className="py-3.5 px-3 text-left text-sm font-semibold text-gray-900"
									>
										{item}
									</th>
								))}
							<th scope="col" className="relative py-3.5 pl-3 pr-4 sm:pr-6">
								<span className="sr-only">Select</span>
							</th>
						</tr>
					</thead>
					<tbody>{body}</tbody>
				</table>
			</div>
		</div>
	);
}
