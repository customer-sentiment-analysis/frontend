import Navbar from './navbar';
import Sidebar from './sidebar';

const Layout = ({ children }: any) => {
	return (
		<div className="w-full h-screen font-mono">
			<div className="flex">
				<div className="flex flex-col w-1/5">
					<Sidebar />
				</div>
				<div className="flex flex-col w-4/5">
					<Navbar />
					<main>{children}</main>
				</div>
			</div>
		</div>
	);
};

export default Layout;
