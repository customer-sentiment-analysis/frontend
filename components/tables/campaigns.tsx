import { formatDate } from '../../utils/functions';
import { IUser } from '../../utils/models/user';

export default function CampaignsList({ data }: any) {
	console.log('Cmp', data);
	return (
		<>
			{!data || data.length == 0 ? (
				<tr>
					<td colSpan={6} className="text-center py-3 text-gray-500">
						Aucune campagne disponible
					</td>
				</tr>
			) : (
				data.map((item: any) => (
					<tr key={item.id} className="px-5">
						<td>
							<div className="text-gray-900 text-center">
								{formatDate(item.created_at)}
							</div>
						</td>
						<td>
							<div className="mt-1 flex flex-col text-gray-500">
								<span>{item.name}</span>
								<span className="text-sm text-gray-300">{item.reference}</span>
							</div>
						</td>
						<td>{formatDate(item.startDate)}</td>
						<td>{item.endDate != null ? formatDate(item.endDate) : ''}</td>
						<td>
							<div>{item.customer.name}</div>
						</td>
						<td className="text-center">{item.status}</td>
						<td className="flex items-center justify-end pr-2"></td>
					</tr>
				))
			)}
		</>
	);
}
