import { formatDate } from '../../utils/functions';

export default function UsersList({ data }: any) {
	return (
		<>
			{data &&
				data.map((item: any) => (
					<tr key={item.id} className="px-5">
						<td>
							<div className="text-gray-900 text-center">
								{formatDate(item.created_at)}
							</div>
						</td>
						<td>
							<div className="mt-1 flex flex-col text-gray-500">
								<span>{item.name}</span>
								<span className="text-sm text-gray-300">{item.reference}</span>
							</div>
						</td>
						<td>{formatDate(item.startDate)}</td>
						<td>{item.endDate != null ? formatDate(item.endDate) : ''}</td>
						<td>
							<div>{item.customer.name}</div>
						</td>
						<td className="text-center">{item.status}</td>
						<td className="flex items-center justify-end pr-2"></td>
					</tr>
				))}
		</>
	);
}
