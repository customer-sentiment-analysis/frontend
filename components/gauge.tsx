import React from 'react';
import ReactEcharts from 'echarts-for-react';

type EChartsOption = echarts.EChartsOption;

const CustomerGauge = () => {
	const option = {
		series: [
			{
				type: 'gauge',
				axisLine: {
					lineStyle: {
						width: 30,
						color: [
							[0.3, '#971B2F'],
							[0.7, '#F0B323'],
							[1, '#509E2F'],
						],
					},
				},
				pointer: {
					itemStyle: {
						color: 'auto',
					},
				},
				axisTick: {
					distance: -30,
					length: 8,
					lineStyle: {
						color: '#fff',
						width: 2,
					},
				},
				splitLine: {
					distance: -30,
					length: 30,
					lineStyle: {
						color: '#fff',
						width: 4,
					},
				},
				axisLabel: {
					color: 'auto',
					distance: 40,
					fontSize: 14,
				},
				detail: {
					valueAnimation: true,
					formatter: '{value}',
					color: 'auto',
				},
				data: [
					{
						value: 83,
					},
				],
			},
		],
	};

	return <ReactEcharts option={option} />;
};

export default CustomerGauge;
