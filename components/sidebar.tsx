import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';

export default function Sidebar() {
	const router = useRouter();
	const currentRoute = router.pathname.split('/')[2];
	const [active, setActive] = useState(currentRoute);

	const activeLink = (link: string) => {
		setActive(link);
		router.push(`/results/${link}`);
	};

	const activeClass = 'bg-theme-dark text-theme-primary font-bold';

	return (
		<div className="flex flex-col shadow-2xl shadow-gray-400 text-gray-700 h-screen  bg-theme-light">
			<div className="flex flex-col mb-8 items-center justify-center bg-theme-dark p-0 m-0 h-16">
				<p className="text-white text-2xl font-codepro">Le Thermomètre</p>
			</div>
			<ul className="flex flex-col gap-2 py-12">
				<li
					onClick={() => activeLink('dashboard')}
					className={`flex gap-2 p-3 font-mono hover:font-bold cursor-pointer ${
						active == 'dashboard'
							? activeClass
							: 'text-theme-dark hover:text-theme-primary hover:bg-theme-dark'
					}`}
				>
					<Link href="/results/dashboard">
						<a className="flex gap-2">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 24 24"
								strokeWidth={1.5}
								stroke="currentColor"
								className="w-6 h-6"
							>
								<path
									strokeLinecap="round"
									strokeLinejoin="round"
									d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z"
								/>
								<path
									strokeLinecap="round"
									strokeLinejoin="round"
									d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z"
								/>
							</svg>

							<span>Tableau de bord</span>
						</a>
					</Link>
				</li>
				<li
					onClick={() => activeLink('campaigns')}
					className={`flex gap-2 p-3 font-mono hover:font-bold cursor-pointer ${
						active == 'campaigns'
							? activeClass
							: 'text-theme-dark hover:text-theme-primary hover:bg-theme-dark'
					}`}
				>
					<Link href="/results/campaigns">
						<a className="flex gap-2">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 24 24"
								strokeWidth="1.5"
								stroke="currentColor"
								className="w-6 h-6"
							>
								<path
									strokeLinecap="round"
									strokeLinejoin="round"
									d="M3.75 4.875c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5A1.125 1.125 0 013.75 9.375v-4.5zM3.75 14.625c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5a1.125 1.125 0 01-1.125-1.125v-4.5zM13.5 4.875c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5A1.125 1.125 0 0113.5 9.375v-4.5z"
								/>
								<path
									strokeLinecap="round"
									strokeLinejoin="round"
									d="M6.75 6.75h.75v.75h-.75v-.75zM6.75 16.5h.75v.75h-.75v-.75zM16.5 6.75h.75v.75h-.75v-.75zM13.5 13.5h.75v.75h-.75v-.75zM13.5 19.5h.75v.75h-.75v-.75zM19.5 13.5h.75v.75h-.75v-.75zM19.5 19.5h.75v.75h-.75v-.75zM16.5 16.5h.75v.75h-.75v-.75z"
								/>
							</svg>

							<span>Campagnes</span>
						</a>
					</Link>
				</li>
				<li
					onClick={() => activeLink('customers')}
					className={`flex gap-2 p-3 font-mono hover:font-bold cursor-pointer ${
						active == 'customers'
							? activeClass
							: 'text-theme-dark hover:text-theme-primary hover:bg-theme-dark'
					}`}
				>
					<Link
						className="flex gap-2 active:text-white"
						href="/results/customers"
					>
						<>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 24 24"
								strokeWidth="1.5"
								stroke="currentColor"
								className="w-6 h-6"
							>
								<path
									strokeLinecap="round"
									strokeLinejoin="round"
									d="M18 18.72a9.094 9.094 0 003.741-.479 3 3 0 00-4.682-2.72m.94 3.198l.001.031c0 .225-.012.447-.037.666A11.944 11.944 0 0112 21c-2.17 0-4.207-.576-5.963-1.584A6.062 6.062 0 016 18.719m12 0a5.971 5.971 0 00-.941-3.197m0 0A5.995 5.995 0 0012 12.75a5.995 5.995 0 00-5.058 2.772m0 0a3 3 0 00-4.681 2.72 8.986 8.986 0 003.74.477m.94-3.197a5.971 5.971 0 00-.94 3.197M15 6.75a3 3 0 11-6 0 3 3 0 016 0zm6 3a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0zm-13.5 0a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0z"
								/>
							</svg>

							<span>Clients</span>
						</>
					</Link>
				</li>
				<li
					onClick={() => activeLink('account')}
					className={`flex gap-2 p-3 font-mono hover:font-bold cursor-pointer ${
						active == 'account'
							? activeClass
							: 'text-theme-dark hover:text-theme-primary hover:bg-theme-dark'
					}`}
				>
					<Link
						className="flex gap-2 active:text-white"
						href="/results/account"
					>
						<>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 24 24"
								strokeWidth="1.5"
								stroke="currentColor"
								className="w-6 h-6"
							>
								<path
									strokeLinecap="round"
									strokeLinejoin="round"
									d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
								/>
							</svg>

							<span>Mon Compte</span>
						</>
					</Link>
				</li>
			</ul>
		</div>
	);
}
