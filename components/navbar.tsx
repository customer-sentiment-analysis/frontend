export default function Navbar() {
	return (
		<div className="h-16 flex bg-theme-secondary font-mono p-3 text-white items-center justify-between">
			<p>Bienvenue, Alioune (ADMIN)</p>
			<p className="shadow-xl shadow-theme-dark bg-red-600 p-2 rounded-3xl hover:bg-red-700 cursor-pointer">
				Se Déconnecter
			</p>
		</div>
	);
}
