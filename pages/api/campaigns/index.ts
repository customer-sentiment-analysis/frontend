// pages/api/todos/index.ts
import type { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../lib/dbConnect";
import { CampaignModel } from "../../../utils/models";
import { ICampaign } from "../../../utils/models/campaign";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await dbConnect();
  if (req.method === "GET") {
    const todos = await CampaignModel.find({}).limit(10).lean();
    res.status(200).json(todos);
  } else if (req.method === "POST") {
    const body = req.body as ICampaign;
    const todo = new CampaignModel({
      name: body.name,
      qrCode: body.qrCode,
      code: body.code,
      slug: body.slug,
      status: body.status,
      company: body.company,
      createdBy: body.createdBy,
      createdAt: body.createdAt,
      startedAt: body.startedAt,
      updatedAt: body.updatedAt,
      endedAt: body.endedAt,
      isActive: body.isActive
    });
    await todo.save();

    res.status(200).json(todo.toJSON());
  } else {
    res.status(405).json({ error: "Method not allowed" });
  }
}
