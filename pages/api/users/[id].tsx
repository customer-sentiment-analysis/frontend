// pages/api/todos/index.ts
import type { NextApiRequest, NextApiResponse } from 'next';
import dbConnect from '../../../lib/dbConnect';
import { UserModel } from '../../../utils/models';
import { IUser } from '../../../utils/models/user';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	await dbConnect();
	const id: string = req.query.id as string;
	if (req.method === 'GET') {
		const todos = await UserModel.findById(id).lean();
		res.status(200).json(todos);
	} else if (req.method === 'PUT') {
		const body = req.body as IUser;
		const todo = await UserModel.findByIdAndUpdate(id, body, { new: true });
		res.status(200).json(todo);
	} else if (req.method === 'DELETE') {
		const todo = await UserModel.findByIdAndRemove(id);
		res.status(200).json(todo);
	} else {
		res.status(405).json({ error: 'Method not allowed' });
	}
}
