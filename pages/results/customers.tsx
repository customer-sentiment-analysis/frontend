import CustomerGauge from '../../components/gauge';
import { CustomerPolarArea } from '../../components/polar-area';
import Layout from '../../components/layout';

const Customers = () => {
	return (
		<Layout>
			<div className="flex bg-gray-100 py-6 text-white items-center justify-between">
				<p>Filters</p>
				<div></div>
			</div>
			<div className="flex flex-wrap m-2">
				<div className="w-1/3">
					<CustomerPolarArea />
				</div>
				<div className="w-1/3 items-center justify-center">
					<CustomerGauge />
					<p className="text-2xl text-center font-codepro">Analytic Gauge</p>
				</div>
			</div>
			<div className="w-3/3 flex items-center justify-center">
				{/*<CustomerThermometer />*/}
			</div>
		</Layout>
	);
};

export default Customers;
