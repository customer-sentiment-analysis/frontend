import CustomerGauge from '../../components/gauge';
import { CustomerPolarArea } from '../../components/polar-area';
import Layout from '../../components/layout';
import CustomerThermometer from '../../components/thermometer';
import SelectBox from '../../components/selectBox';
import { useState } from 'react';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import StatsBox from '../../components/dashboard/statsBox';

const people = [
	{
		name: 'Yakhya DIOP',
		username: 'Beyd Group',
		campaigns: [
			{ name: 'Sondage Service Bancaire BOA' },
			{ name: 'Enquête de satisfaction BeydMarket' },
			{ name: 'Devon Webb' },
			{ name: 'Tom Cook' },
		],
	},
	{ name: 'Arlene Mccoy', username: '@arlenemccoy' },
	{ name: 'Devon Webb', username: '@devonwebb' },
	{ name: 'Tom Cook', username: '@tomcook' },
	{ name: 'Tanya Fox', username: '@tanyafox' },
	{ name: 'Hellen Schmidt', username: '@hellenschmidt' },
	{ name: 'Caroline Schultz', username: '@carolineschultz' },
	{ name: 'Mason Heaney', username: '@masonheaney' },
	{ name: 'Claudie Smitham', username: '@claudiesmitham' },
	{ name: 'Emil Schaefer', username: '@emilschaefer' },
];

const Dashboard = () => {
	const [dateRange, setDateRange] = useState([null, null]);
	const [startDate, endDate] = dateRange;

	return (
		<Layout>
			<div className="flex">
				<div className="flex flex-col py-6 px-4">
					<fieldset className="shadow shadow-theme-dark p-5">
						<legend className="text-lg font-bold">Filtres</legend>
						<div className="flex flex-col gap-2">
							<SelectBox title={'Clients'} data={people} />
							<SelectBox title={'Campagnes'} data={people[0].campaigns} />
							<div className="flex flex-col gap-2">
								<label className="mx-2">Date</label>
								<DatePicker
									className="border-2 border-theme-gray-300 px-4 py-2 rounded w-full"
									selectsRange={true}
									dateFormat="dd/MM/yyyy"
									onChange={(update) => {
										setDateRange(update);
									}}
									startDate={startDate}
									endDate={endDate}
									withPortal
								/>
							</div>
						</div>
					</fieldset>
				</div>
				<StatsBox />
			</div>
			<div className="flex flex-wrap m-2">
				<div className="w-1/2">
					<CustomerPolarArea />
				</div>
				<div className="w-1/2 items-center justify-center">
					<CustomerGauge />
					<p className="text-2xl text-center">Analytic Gauge</p>
				</div>
			</div>
			<div className="flex items-center justify-center">
				{/* <CustomerThermometer /> */}
			</div>
		</Layout>
	);
};

export default Dashboard;
