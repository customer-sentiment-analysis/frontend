import CustomerGauge from '../../components/gauge';
import { CustomerPolarArea } from '../../components/polar-area';
import Layout from '../../components/layout';
import SupabaseService from '../../utils/supabseInit';
import { useEffect, useState } from 'react';
import DataTable from '../../components/datatable';
import CampaignsList from '../../components/tables/campaigns';
import { IUser } from '../../utils/models/user';

const Accounts = () => {
	const [users, setUsers] = useState([]);
	const tableHead = ['Créé Le', 'Prénom', 'Nom', 'email', 'role', 'Statut'];

	const getUsers = async () => {
		let users = await fetch('/api/users');
		const data = await users.json();
		// setCampaigns({data});
		console.log('====================================');
		console.log(data);
		// console.log(error);
		console.log('====================================');
		setUsers(data);
	};

	const addUser = async (data: IUser) => {
		const add = await fetch('/api/users', {
			method: 'POST',
			body: JSON.stringify(data),
		});
		console.log('====================================');
		console.log(data);
		console.log('====================================');
	};

	useEffect(() => {
		// addcampaign();
		getUsers();
	}, []);

	return (
		<Layout>
			<div className="font-serif">
				<DataTable
					title={'Campagnes / Enquétes de Satisfaction'}
					headers={tableHead}
					body={<CampaignsList data={users} />}
				/>
			</div>
		</Layout>
	);
};

export default Accounts;
