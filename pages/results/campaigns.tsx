import CustomerGauge from '../../components/gauge';
import { CustomerPolarArea } from '../../components/polar-area';
import Layout from '../../components/layout';
import SupabaseService from '../../utils/supabseInit';
import { useEffect, useState } from 'react';
import DataTable from '../../components/datatable';
import CampaignsList from '../../components/tables/campaigns';

const Campaigns = () => {
	const [campaigns, setCampaigns] = useState([]);
	const tableHead = [
		'Créé Le',
		'Titre',
		'Date début',
		'Date Fin',
		'Client',
		'Statut',
	];
	const supabase = SupabaseService();

	const getCampaigns = async () => {
		let campaign = await fetch(
			`${process.env.NEXT_PUBLIC_THERMO_API}/campaigns`
		);
		const data = await campaign.json();
		// setCampaigns({data});
		console.log('====================================');
		console.log('Fii', data);
		// console.log(error);
		console.log('====================================');
		setCampaigns(data);
	};

	const addcampaign = async () => {
		const { data, error } = await supabase.from('campaign').insert([
			{
				name: 'Fist Campaign',
				description: 'Test la',
				startDate: new Date().toISOString(),
				status: 'Pending',
				customer_id: 1,
			},
		]);
		console.log('====================================');
		console.log(data);
		console.log(error);
		console.log('====================================');
	};

	useEffect(() => {
		// addcampaign();
		getCampaigns();
	}, []);

	return (
		<Layout>
			<div className="font-serif">
				<DataTable
					headers={tableHead}
					body={<CampaignsList data={campaigns} />}
				/>
			</div>
		</Layout>
	);
};

export default Campaigns;
