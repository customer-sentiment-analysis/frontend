/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./pages/**/*.{js,ts,jsx,tsx}',
		'./components/**/*.{js,ts,jsx,tsx}',
	],
	theme: {
		extend: {
			fontFamily: {
				codepro: ['Source Code Pro', 'monospace'],
			},
			colors: {
				primaire: '#6567F6',
				secondaire: '#66CD9D',
				titles: '#1D2938',
				content: '#8E949B',
				background: '#F2F2F2',
				'emotion-positive': '#509E2F',
				'emotion-neutral': '#F0B323',
				'emotion-negative': '#971B2F',
				'theme-light': '#E4F9F5',
				'theme-dark': '#40514E',
				'theme-primary': '#30E3CA',
				'theme-secondary': '#11999E',
			},
		},
	},
	plugins: [],
};
