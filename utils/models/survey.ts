import { prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { nanoid } from "nanoid";

export class Survey extends TimeStamps {
  @prop({ default: () => nanoid(32) })
  _id: string;

  @prop()
  sessionId: string;

  @prop()
  sessionData: string[];

  @prop()
  sessionDataContentType: string;

  @prop()
  duration: number;

  @prop()
  sessionStart: Date;

  @prop()
  sessionEnd: Date;

}
export interface ISurvey {
  _id?: string;
  // answer: string;
  sessionId: string;
  sessionData: any;
  sessionDataContentType: string;
  sessionStart: Date;
  sessionEnd: Date;
  duration: number;
  // question: Question;
}
