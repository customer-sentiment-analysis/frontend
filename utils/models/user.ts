import { Ref, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { nanoid } from "nanoid";
import { Company } from "./company";

export class User extends TimeStamps {
  @prop({ default: () => nanoid(32) })
  _id: string;

  @prop()
  firstName: string;

  @prop()
  lastName: string;

  @prop()
  address: string;

  @prop()
  phone: string;

  @prop()
  email: string;

  @prop()
  role: string;

  @prop()
  isActive: boolean;

  @prop({ ref: () => Company })
  company?: Ref<Company>

}

export interface IUser {
  _id: string;
  firstName: string;
  lastName: string;
  address: string;
  phone: string;
  email: string;
  role: string;
  isActive: boolean;
  company?: any
}