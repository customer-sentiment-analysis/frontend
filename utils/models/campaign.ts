import { prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { nanoid } from "nanoid";
import { User } from "./user";
import { Company } from "./company";
import { Question } from "./question";

export class Campaign extends TimeStamps {
  @prop({ default: () => nanoid(32) })
  _id: string;

  @prop()
  name: string;

  @prop()
  code: string;

  @prop()
  qrCode: string;

  @prop()
  slug: string;

  @prop()
  status: string;

  @prop({ default: false })
  isActive: boolean;

  @prop()
  startedAt: Date;

  @prop()
  endedAt: Date;

  @prop({ ref: () => User })
  createdBy?: Ref<User>

  @prop({ ref: () => Company })
  company?: Ref<Company>

  @prop({ ref: () => Question })
  questions: Ref<Question>[];
}

export interface ICampaign {
  _id?: string;
  name: string;
  code: string;
  qrCode: string;
  slug: string;
  startedAt: Date;
  createdAt: Date;
  createdBy: any;
  updatedAt: Date;
  endedAt: Date;
  status: string;
  isActive: boolean;
  company: any;
  questions: any[]
}
