import { getModelForClass } from '@typegoose/typegoose';
import { Campaign } from './campaign';
import { Company } from './company';
import { Question } from './question';
import { Survey } from './survey';
import { User } from './user';

export const CampaignModel = getModelForClass(Campaign);
export const CompanyModel = getModelForClass(Company);
export const QuestionModel = getModelForClass(Question);
export const SurveyModel = getModelForClass(Survey);
export const UserModel = getModelForClass(User);
