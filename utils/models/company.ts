import { prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { nanoid } from "nanoid";
import { User } from "./user";

export class Company extends TimeStamps {
  @prop({ default: () => nanoid(32) })
  _id: string;

  @prop()
  name: string;

  @prop()
  description: string;

  @prop()
  address: string;

  @prop()
  phone: string;

  @prop()
  email: string;

  @prop()
  isActive: boolean;

  @prop({ ref: () => User })
  manager?: Ref<User>

}

export interface ICompany {
  _id?: string;
  name: string;
  description: string;
  address: string;
  phone: string;
  email: string;
  isActive: boolean;
  manager?: any
}