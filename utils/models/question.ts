import { Campaign } from "./campaign";

import { Ref, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { nanoid } from "nanoid";

export class Question extends TimeStamps {
  @prop({ default: () => nanoid(32) })
  _id: string;

  @prop()
  content: string;

  @prop({ default: false })
  isActive: boolean;

  @prop({ ref: () => Campaign })
  campaign: Ref<Campaign>;

}

export interface IQuestion {
  _id?: string;
  content: string;
  createdAt: Date;
  isActive: boolean;
  campaign: Campaign;
}
