import Moment from 'moment';

export function formatDate(date: any) {
	return Moment(date).format('DD/MM/YYYY');
}

export function randomString (length: number) {
	let s = '';
	Array.from({ length }).some(() => {
		s += Math.random().toString(36).slice(2);
		return s.length >= length;
	});
	return s.slice(0, length);
};
