export class CampaignService {

  getCampaign = (qrcode: any) => {
    return fetch(`${process.env.NEXT_PUBLIC_THERMO_API}/campaigns/qr/${qrcode}`)
  }

  sendResponse = (payload: any) => {
    return fetch(`${process.env.NEXT_PUBLIC_THERMO_API}/survey-data`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
      }
    })
  }

}
